```markdown
## Naziv funkcionalnosti: Prijava (Login)

### Opis
Ovaj endpoint se koristi za autentikaciju i prijavljivanje korisnika.

### URL
POST /api/user/sign-up/login

### Header
- Content-Type: application/json

### Request Body
```json
{
    "username": "user",
    "password": "pass"
}
```

### Response Body
```json
{
    "token": "Core eyJhbGciOiJIUzUxMiJ9...",
    "refreshToken": "eyJhbGciOiJIUzUxMiJ9..."
}
```

### Statusni kodovi
- 200 OK: Prijava uspešna.
- 400 Bad Request: Pogrešno korisničko ime ili lozinka.
- 401 Unauthorized: Neovlašćen pristup.
- 500 Internal Server Error: Server je naišao na grešku.

## Funkcionalnost: Prikaz organizacionih jedinica iz aktivne strukture filtrirano po organu

### Request
- Endpoint: api/organizational-unit/get-org-unit-tree/<url organa TBD>
- Header:
  - Authorization: Bearer token.

### Response
```json
{
	"type": "10, Министарство, SV9",
	"identificationNumber": "88996446",
	"jbkjs": "8752221455",
	"pib": "7896544122",
	"name": "Demo organ",
	"note": null,
	"version": "ACTIVE",
	
	"contains": [
		{
			"type": "381, Сектор, SV703",
			"identificationNumber": "45698556",
			"jbkjs": null,
			"pib": null,
			"name": "Organizaciona jedinica",
			"note": null,
			"parentId": 1589,
			"authority": false
		}
	],
	"authority": true
}
```

## Funkcionalnost: Prikaz radnih mesta filtrirano po organu

### Endpoint
/api/position/0/10 <0/10 – pageable parametri>

### Request
- Header:
  - Authorization: Bearer [token]

### Response
```json
{
    "content": [
        {
            "name": "RadnoMesto15596497",
            "numberOfExecutors": 0,
            "code": "15596497",
            "type": "Извршилачко радно место",
            "managerial": false,
            "organizationalUnit": "Organ42493301",
            "numberOfEmployeeDossiers": 0,
            "heavyFieldOfWork": null
        },
        {
            "name": "RadnoMesto70574624",
            "numberOfExecutors": 0,
            "code": "70574624",
            "type": "Извршилачко радно место",
            "managerial": false,
            "organizationalUnit": "Organ59560182",
            "numberOfEmployeeDossiers": 0,
            "heavyFieldOfWork": null,
            "active": true
        }
    ],
    "pageable": {
        "sort": {
            "empty": true,
            "sorted": false,
            "unsorted": true
        },
        "offset": 0,
        "pageSize": 10,
        "pageNumber": 0,
        "unpaged": false,
        "paged": true
    },
    "totalPages": 1,
    "totalElements": 2,
    "last": true,
    "size": 10,
    "number": 0,
    "sort": {
        "empty": true,
        "sorted": false,
        "unsorted": true
    },
    "numberOfElements": 2,
    "first": true,
    "empty": false
}
```

## Funkcionalnost: Pregled pojedinačnog korisnika po LIB-u

### Request
- Metoda: GET
- Endpoint: api/personal-data/57
- Header:
  - Authorization: Bearer [token]

### Response
```json
[
    {
        "firstname": "Петар",
        "lastname": "Петровић",
        "parentName": "Горан",
        "jmbg": "2213937710131",
        "dateOfBirth": "1987-02-23T10:52:06.000+01:00",
        "placeOfBirth": "Београд",
        "countryOfBirth": "Србија",
        "employeeDossier": {
            "lib": "11111154",
            "holidayNumberOfDays": null,
            "holidayNumberOfDaysInPastYear": null
        },
        "addressType": "Пребивалиште",
        "street": "Булевар Михаила Пупина 147",
        "floor": "16",
        "municipality": "Београд",
        "pttNumber": "11000",
        "city": "Београд",
        "country": "Србија",
        "contactType": "Електронска пошта",
        "contact": "petar.petrovic@suk.gov.rs"
    }
]
```
```